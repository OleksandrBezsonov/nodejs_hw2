const { Note } = require('./models/Notes.js');

function createNote(req, res, next) {
  try {
    const { text } = req.body;
    const note = new Note({
      userId: req.user.userId,
      completed: false,
      text,
      createdDate: new Date()
    });
    note.save().then((saved) => {
      res.status(200).send({
        message: 'Success'
      });
    });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

function getNotes(req, res, next) {
  return Note.find().then((result) => {
    res.json(result);
  });
}

const getNote = (req, res, next) => {
  try{
    Note.findById(req.params.id)
    .then((note) => {
      res.json({'note': note});
    });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

const updateNote = async (req, res, next) => {
  const note = await Note.findById(req.params.id);
  const { text } = req.body;

  if (title) note.title = title;
  if (author) note.author = author;

  return note.save().then((saved) => res.json({massage: 'Successfull', saved}));
};

const deleteNote = (req, res, next) => {
  try {
    Note.findByIdAndDelete(req.params.id)
    .then((note) => {
      res.json({message: 'Success'});
    });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

const getMyNotes = (req, res, next) => {
  try{console.log(req.query)
    Note.find({ userId: req.user.userId }, '-__v')
  .skip(req.query.offset)
  .limit(req.query.limit)
  .then((result) => {
  res.json({limit: req.params.limit, notes: result});
  });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}

const updateMyNoteById = (req, res, next) => {
  try { const { text } = req.body;
  return Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { text } })
    .then((result) => {
      res.json({ message: 'Success' });
    }); 
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const markMyNoteCompletedById = async (req, res, next) => {
  try { 
    const note = await Note.findById(req.params.id);
    console.log(note.completed);
    if (note.completed == false){
      Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { completed: true } })
    .then((result) => {
      res.status(200).send({
        message: 'Success'
      });
    })
    .catch((err) => {
      next(err);
    });
  } else {
    Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { completed: false } })
    .then((result) => {
      res.status(200).send({
        message: 'Success'
      });
    })
    .catch((err) => {
      next(err);
    });
  }
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
}
module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  deleteNote,
  getMyNotes,
  updateMyNoteById,
  markMyNoteCompletedById,
};
