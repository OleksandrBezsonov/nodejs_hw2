const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users.js');

function getProfile(req, res, next){
    try{
        User.findById(req.user.userId)
        .then((user) => {
          res.json({'user': {
            "_id": user.id,
            "username": user.username,
            "createdDate": user.createdDate
          }})
        })
        .catch((err) => {
            next(err);
          });
      } catch (err) {
        return res.status(400).json({ message: err.message });
      }
}

function deleteProfile(req, res, next){
    try {
        User.findByIdAndDelete(req.user.userId)
        .then((user) => {
          res.json({message: 'Success'});
        })
        .catch((err) => {
            next(err);
          });
      } catch (err) {
        return res.status(400).json({ message: err.message });
      }
}

async function changePassword(req, res, next){
    try { const { oldPassword, newPassword } = req.body;
    const user = await User.findById(req.user.userId);
    if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
        User.findByIdAndUpdate(req.user.userId, { $set: { password: await bcrypt.hash(newPassword, 10)} })
        .then((result) => {
            res.json({message: 'Success'});
          })
          .catch((err) => {
              next(err);
            });
    } else {
        return res.status(400).json({ message: err.message });
    }
    } catch (err) {
        return res.status(400).json({ message: err.message });
    }
}

module.exports = {
    getProfile,
    deleteProfile,
    changePassword
};