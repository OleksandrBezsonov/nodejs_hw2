const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://alex:bess@cluster1.tuuq1i0.mongodb.net/test');

const { notesRouter } = require('./notesRouter.js');
const { authRouter } = require('./authRouter.js');
const { usersRouter } = require('./usersRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
